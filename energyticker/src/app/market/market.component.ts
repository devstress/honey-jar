import { Component, OnInit } from '@angular/core';
import Eos from 'eosjs';

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.scss']
})
export class MarketComponent implements OnInit {
  honey;
  constructor() {
    var that = this;
    const eos = Eos({ httpEndpoint: 'http://172.16.96.176:8888' });
    eos.getTableRows({
      "json": true,
      "code": "powerapp",   // contract who owns the table
      "scope": "powerapp",  // scope of the table
      "table": "accounts",    // name of the table as specified by the contract abi
      "limit": 100,
    }).then(result => {
      that.honey = result.rows.find(x => x.accountname == localStorage.getItem('currentUserID')).honey_amount;
    });
  }

  ngOnInit() {
  }

}
