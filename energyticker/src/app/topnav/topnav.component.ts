import { Component, OnInit } from '@angular/core';
import Eos from 'eosjs';

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.scss']
})
export class TopnavComponent implements OnInit {
  ngOnInit(): void {
  }

  currentUser = '';
  users:string[];

  constructor() {
    var that = this;
    const eos = Eos({ httpEndpoint: 'http://172.16.96.176:8888' });
        var that = this;
        eos.getTableRows({
          "json": true,
          "code": "powerapp",   // contract who owns the table
          "scope": "powerapp",  // scope of the table
          "table": "accounts",    // name of the table as specified by the contract abi
          "limit": 100,
        }).then(result => {
          var user = result.rows.find(x => x.note == 'user');
          localStorage.setItem('currentUserID', user.accountname);
          localStorage.setItem('currentUserPrivateKey', '5K78THY96F2e2MAqYo2NLrW762zZ2XPFFMWLwzA5nw7fWvneXoa');
          that.currentUser = localStorage.getItem('currentUserID');
          that.users = result.rows.filter(x => x.note == 'user' || x.note == 'sponsor').map(x => x.accountname);
    });
  }

  changeUser(event, user) {
    this.currentUser = user;
    localStorage.setItem('currentUserID', user);
  }
}
