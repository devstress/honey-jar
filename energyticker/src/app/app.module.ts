import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { LandingComponent } from './landing/landing.component';
import { MaterialModule } from './material/material.module';
import { NavComponent } from './nav/nav.component';
import { FormsModule } from '../../node_modules/@angular/forms';
import { WalletComponent } from './wallet/wallet.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { MarketComponent } from './market/market.component';
import { DonateComponent } from './donate/donate.component';
import { TopnavComponent } from './topnav/topnav.component';


@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    NavComponent,
    LandingComponent,
    WalletComponent,
    LeaderboardComponent,
    MarketComponent,
    DonateComponent,
    TopnavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
