export class Picture {
    pictures: string[];
    constructor(speciesid: number, randomnums: number[]) {
        if (speciesid == 1) {
            this.pictures = ['assets/images/worker.png'];
        } else if (speciesid == 2) {
            this.pictures = ['assets/images/bumblebee.png'];
        } else {
            var firstPartNo = randomnums[0] % 4;
            var secondPartNo = randomnums[1] % 4;
            var thirdPartNo = randomnums[2] % 4;
            var firstPart = firstPartNo == 0 ? 'assets/images/queen_wing_1.png' :
                firstPartNo == 1 ? 'assets/images/queen_wing_2.png' :
                    firstPartNo == 2 ? 'assets/images/queen_wing_3.png' : 'assets/images/queen_wing_4.png';
            var secondPart = secondPartNo == 0 ? 'assets/images/queen_body_1.png' :
                secondPartNo == 1 ? 'assets/images/queen_body_2.png' :
                    secondPartNo == 2 ? 'assets/images/queen_body_3.png' : 'assets/images/queen_body_4.png';
            var thirdPart = thirdPartNo == 0 ? 'assets/images/queen_eyes_1.png' :
                thirdPartNo == 1 ? 'assets/images/queen_eyes_2.png' :
                    thirdPartNo == 2 ? 'assets/images/queen_eyes_3.png' : 'assets/images/queen_eyes_4.png';
            this.pictures = [firstPart, secondPart, thirdPart];
        }
    }
}