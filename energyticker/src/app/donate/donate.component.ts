import { Component, OnInit } from '@angular/core';
import Eos from 'eosjs';
import { Picture } from '../common/picture';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.scss']
})
export class DonateComponent implements OnInit {
  isOpen = false;
  isShaking = false;
  animal;
  pictures = [];
  constructor() { }

  ngOnInit() {

  }

  async donate() {
    this.pictures = [];
    this.isOpen = false;
    this.isShaking = true;
    this.animal = "";
    var that = this;
    var id = new Date().valueOf();

    const eos = Eos({ keyProvider: localStorage.getItem('currentUserPrivateKey'), httpEndpoint: 'http://172.16.96.176:8888' });

    let actionData = {
      catcher: localStorage.getItem('currentUserID'),
      specialnumber: id,
      name: ''
    };

    await eos.transaction({
      actions: [{
        account: 'powerapp',
        name: 'catchex',
        authorization: [{
          actor: localStorage.getItem('currentUserID'),
          permission: 'active',
        }],
        data: actionData,
      }],
    });

    setTimeout(function () {
      const eos = Eos({ httpEndpoint: 'http://172.16.96.176:8888' });
      eos.getTableRows({
        "json": true,
        "code": "powerapp",   // contract who owns the table
        "scope": "powerapp",  // scope of the table
        "table": "animals",    // name of the table as specified by the contract abi
        "limit": 100,
      }).then(result => {
        that.isOpen = true;
        that.isShaking = false;
        var animal = result.rows.find(x => x.specialnum == id && x.owner == localStorage.getItem('currentUserID'));
        that.pictures = new Picture(animal.speciesid, animal.randomnums).pictures;
        //that.pictures = new Picture(3, [1, 2, 3]).pictures;
        console.log(that.pictures);
      });
    }, 1000);
  }
}
