import { Component, OnInit } from '@angular/core';
import Eos from 'eosjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor() { }


  account: string;
  privateKey: string;
  note: string;
  result;

  ngOnInit() {

  }

  async onSubmit() {
    // collect form data
    const accountValue = this.account || 'useraaaaaaaa';
    const privateKeyValue = this.privateKey || '5K78THY96F2e2MAqYo2NLrW762zZ2XPFFMWLwzA5nw7fWvneXoa';
    const noteValue = this.note || 'test';

    // define actionName and action according to event type
    let actionData = {
      lename: accountValue,
      lenote: noteValue,
    };

    // eosjs function call: connect to the blockchain
    const eos = Eos({ keyProvider: privateKeyValue, httpEndpoint: 'http://172.16.96.176:8888' });
    try {
      const result = await eos.transaction({
        actions: [{
          account: 'powerapp',
          name: 'crtaccnt',
          authorization: [{
            actor: accountValue,
            permission: 'active',
          }],
          data: actionData,
        }],
      });
      this.result = result;
      console.log(result);
    } catch (e) {
      this.result = e;
      console.log(e);
    }
  }

  retrieveUsers() {
    const eos = Eos({ httpEndpoint: 'http://172.16.96.176:8888' });
    var that = this;
    eos.getTableRows({
      "json": true,
      "code": "powerapp",   // contract who owns the table
      "scope": "powerapp",  // scope of the table
      "table": "accounts",    // name of the table as specified by the contract abi
      "limit": 100,
    }).then(result => {
      that.result = result;
      console.log(result);
    });
  }

  retrieveSpecies() {
    const eos = Eos({ httpEndpoint: 'http://172.16.96.176:8888' });
    var that = this;
    eos.getTableRows({
      "json": true,
      "code": "powerapp",   // contract who owns the table
      "scope": "powerapp",  // scope of the table
      "table": "specieses",    // name of the table as specified by the contract abi
      "limit": 100,
    }).then(result => {
      that.result = result;
      console.log(result);
    });
  }

  retrieveAnimals() {
    const eos = Eos({ httpEndpoint: 'http://172.16.96.176:8888' });
    var that = this;
    eos.getTableRows({
      "json": true,
      "code": "powerapp",   // contract who owns the table
      "scope": "powerapp",  // scope of the table
      "table": "animals",    // name of the table as specified by the contract abi
      "limit": 100,
    }).then(result => {
      that.result = result;
      console.log(result);
    });
  }
}