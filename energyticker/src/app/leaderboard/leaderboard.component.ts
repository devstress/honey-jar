import { Component, OnInit } from '@angular/core';
import Eos from 'eosjs';
import { Picture } from '../common/picture';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {
  rankings = [];
  /**
   * README: because marketplace is under construction so we cannot 
   * calculate correct the value based on market price.
   * So we will mock it in this class with estimated value only.
   */
  constructor() {
    const eos = Eos({ httpEndpoint: 'http://172.16.96.176:8888' });
    var that = this;
    eos.getTableRows({
      "json": true,
      "code": "powerapp",   // contract who owns the table
      "scope": "powerapp",  // scope of the table
      "table": "animals",    // name of the table as specified by the contract abi
      "limit": 1000,
    }).then(result => {
      var ranking1 = result.rows.filter(x => x.speciesid == 2).map(x => {
        x.honey = that.randomIntFromInterval(1000, 1200);
        x.pictures = new Picture(x.speciesid, x.randomnums).pictures;
        return x;
      }).sort((x, y) => x.honey < y.honey);
      var ranking2 = result.rows.filter(x => x.speciesid == 3).map(x => {
        x.honey = that.randomIntFromInterval(100, 250);
        x.pictures = new Picture(x.speciesid, x.randomnums).pictures;
        return x;
      }).sort((x, y) => x.honey < y.honey).slice(1, 100 - ranking1.length);
      var ranking3 = result.rows.filter(x => x.speciesid == 1).map(x => {
        x.honey = that.randomIntFromInterval(1, 5);
        x.pictures = new Picture(x.speciesid, x.randomnums).pictures;
        return x;
      }).sort((x, y) => x.honey < y.honey).slice(1, 100 - ranking1.length - ranking2.length);
      that.rankings = ranking1.concat(ranking2, ranking3);
    });
  }

  ngOnInit() {
  }

  randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}
