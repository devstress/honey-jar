import { Component, OnInit } from '@angular/core';
import Eos from 'eosjs';
import { Picture } from '../common/picture';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {

  constructor() {
    this.retrieveAnimals();
  }
  account: string;
  privateKey: string;
  note: string;
  result;
  animals;
  honey;
  ngOnInit() {
  }
  retrieveAnimals() {
    const eos = Eos({ httpEndpoint: 'http://172.16.96.176:8888' });
    var that = this;
    eos.getTableRows({
      "json": true,
      "code": "powerapp",   // contract who owns the table
      "scope": "powerapp",  // scope of the table
      "table": "animals",    // name of the table as specified by the contract abi
      "limit": 100,
    }).then(result => {
      that.result = result;
      that.animals = result.rows.filter(x => x.owner == localStorage.getItem('currentUserID'));
      that.animals = that.animals.map(x => {
        x.pictures = new Picture(x.speciesid, x.randomnums).pictures;
        return x;
      });
      eos.getTableRows({
        "json": true,
        "code": "powerapp",   // contract who owns the table
        "scope": "powerapp",  // scope of the table
        "table": "accounts",    // name of the table as specified by the contract abi
        "limit": 100,
      }).then(result => {
        that.honey = result.rows.find(x => x.accountname == localStorage.getItem('currentUserID')).honey_amount;
      });
    });
  }

  async turnToHoney(event, animal) {
    const eos = Eos({ keyProvider: localStorage.getItem('currentUserPrivateKey'), httpEndpoint: 'http://172.16.96.176:8888' });

    let actionData = {
      animalid: animal.animalid
    };

    await eos.transaction({
      actions: [{
        account: 'powerapp',
        name: 'burnanimal',
        authorization: [{
          actor: localStorage.getItem('currentUserID'),
          permission: 'active',
        }],
        data: actionData,
      }],
    });

    this.retrieveAnimals();
    console.log('burnt animal ' + animal.animalid);
  }
}
