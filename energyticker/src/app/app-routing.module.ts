import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DonateComponent } from './donate/donate.component';
import { FormComponent } from './form/form.component';
import { LandingComponent } from './landing/landing.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { MarketComponent } from './market/market.component';
import { WalletComponent } from './wallet/wallet.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    data: { title: 'Welcome to Honey Jar' }
  },
  {
    path: 'donate',
    component: DonateComponent,
    data: { title: 'Donate' }
  },
  {
    path: 'wallet',
    component: WalletComponent,
    data: { title: 'Your animal tokens' }
  },
  {
    path: 'leaderboard',
    component: LeaderboardComponent,
    data: { title: 'Leaderboard' }
  },
  {
    path: 'market',
    component: MarketComponent,
    data: { title: 'Market Place' }
  },
  {
    path: 'dashboard',
    component: FormComponent,
    data: { title: 'Dashboard' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
