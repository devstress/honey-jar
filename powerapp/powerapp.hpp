#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
using namespace eosio;
using namespace std;
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
#include "hackyhelpers.hpp"
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
class powermon{
   public:
      
};
////////////////////////////////////////////////////////////////////////////
class powerapp : public eosio::contract{
   public:
      using contract::contract;
      
      //@abi table metadata
      class metadata{
         public:
            int64_t sponsorcounter;
            int64_t speciescounter;
            int64_t animalcounter;
            
            vector<checksum256> seedhashes;
            
            void init();
            
            int64_t primary_key() const{ return 0;  };
            
      };
      
      //@abi table specieses
      class species{
         public:
            int64_t speciesid;
            string name; // e.g koala
            string description;
            account_name sponsor; // 0 by default, means it's unsponsored
            int32_t commonness; // relative number out of all of these
            
            void init();
            
            int64_t primary_key() const { return speciesid; };
      };
      
      //@abi table animals
      class animal{
         public:
            int64_t animalid;
            int64_t speciesid; // e.g koala
            uint64_t specialnum;
            account_name owner;
            string name;
            vector<int8_t> randomnums;
            
            int64_t estivalue;
            
            void init();
            
            int64_t primary_key() const { return animalid; };
            uint64_t specnum() const { return specialnum; };
            uint64_t ownerkey() const { return owner; };
            
      };
      
      //@abi table accounts
      class account{
         public:
            account_name accountname;
            std::string note;
            int64_t honey_amount;
            
            void init();
            
            uint64_t primary_key() const{
               return accountname;
            };
            
      };
      
      
      class honeynomics{
         public:
            uint64_t supply;
            uint64_t distributed; // to users
            
            void init();
            
            uint64_t primary_key() const{
               return 0;
            };
            
      };
      
      
      
      //@abi table
      shingleton(metadata) METADATA;
      shingleton(honeynomics) HONEYNOMICS;
      shingmapp(accounts, account) ACCOUNTS;
      shingmapp2(animals, animal, specnum, specnum, ownerkey, ownerkey) ANIMALS;
      shingmapp(specieses, species) SPECIESES;
      
      void clearstuff();
      void addrand(checksum256 lehash);
      void declspeci(const string& name, const string& desc, account_name sponsor, int32_t commonness);
      void speciredesc(int64_t speciesid, const string& thedesc);
      
      void qckusrsetup(account_name lename, string lenote, int64_t lescore);
      
      void genocide();
      // for demo purposes only:
      // catching animal does not consume bee, where it should be a transaction instead
      void catchex(account_name catcher, const string& name, uint64_t specialnumber);
      void catchanimal(account_name catcher, const string& name, checksum256 lehash, uint64_t specialnumber);
      void makespecial(account_name catcher, const string& name, int64_t speciesnum);
      // catching animal, kinda works, yeah
      
      void clraccnts();
      void crtaccnt(account_name lename, string lenote);
      
      void createhoney(uint64_t amount);
      void burnhoney(uint64_t amount);
      void issuehoney(account_name lename, uint64_t amount);
      void burnanimal(uint64_t animalid);
      void movehoney(account_name from, account_name to, uint64_t amount);
      
      
      // not part of ABI
      void maybeCreateAccount(account_name lename);
      
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////